---
title: Kickoff for [date]
published: true
---

# Kickoff For [date]

Let's get this Monday started with these links:

<!--more-->

## Category 1

**[]()**, wherein ...

**[]()**, wherein ...

**[]()**, wherein ...

## Category 2

**[]()**, wherein ...

**[]()**, wherein ...

**[]()**, wherein ...

## Category 3

**[]()**, wherein ...

**[]()**, wherein ...

**[]()**, wherein ...

And that's it for this Monday. Come back in seven days for another set of links to start off your week.

&mdash; [Scott Nesbitt](https://scottnesbitt.net)

---

If you enjoy The Monday Kickoff, please consider supporting it by [buying me a coffee](https://ko-fi.com/A335465D) or making a micropayment via [Liberapay](https://liberapay.com/scottnesbitt/) or [PayPal](https://paypal.me/scottnesbitt). Even if you don't, I'll keep doing this. Your support (even if you just read this space) is appreciated!

---

<small>
The Monday Kickoff is licensed under <a href="https://creativecommons.org/publicdomain/zero/1.0/" target="_blank">CC0 Public Domain</a>. <img src="https://scottnesbitt.net/img/cc0-88x31.png" "CC0" align="middle" />
</small>
